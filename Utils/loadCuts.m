function [begin_cut, end_cut] = loadCuts(cutwithpath, timestamp)
% function [begin_cut, end_cut] = loadCuts(cutwithpath, timestamp)
% load the cuts for a datasetfile with the timestamp from ecgcuts_db
% input: file, timestamp
% output: cut at begin, cut at end
%% Authors: Kilin Shi, Sven Schellenberger
% Copyright (C) 2017  Kilin Shi, Sven Schellenberger

% find sync info for a manually loaded Datafile
% Bsp: timestamp: '2016-12-21_14-44-34'
if isempty(cutwithpath) || ~exist(cutwithpath, 'file')
   error('no sync file') 
end

fileID = fopen(cutwithpath,'r');
data = textscan(fileID,'%s %s %s', 'Delimiter', ';');
fclose(fileID);

if isempty(data)
   error('empty sync file'); 
end

db_names = data{1}(2:end,1); % cut column name, timestamp identifies file
db_begin_cut = data{2}(2:end,1); 
db_end_cut = data{3}(2:end,1); 
ind = find(strcmp(db_names,timestamp));
    if ind
        begin_cut = db_begin_cut{ind};
        end_cut = db_end_cut{ind};
    else
        begin_cut = 1;
        end_cut = -1;
        warning('Timestamp not found in ecgcuts_db!'); 
    end
end